'use strict';
var app = angular.module('comics', [
  'ngRoute',
  'ngCookies',
  'comics.controllers',
  'comics.filters',
  'comics.services'
]);

app.config(['$routeProvider', function ($routeProvider) {
  $routeProvider
  .when('/', {
    templateUrl: 'views/login.html',
    controller: 'loginController'
  })
  .when('/register', {
    templateUrl: 'views/register.html',
    controller: 'loginController'
  })
  .when('/comics', {
    templateUrl: 'views/comics.html',
    controller: 'comicsController'
  })
  .when('/newcomic', {
    templateUrl: 'views/newcomic.html',
    controller: 'comicsController'
  })
  .when('/comic/:id', {
    templateUrl: 'views/comic.html',
    controller: 'comicController'
  })
  .when('/employees', {
    templateUrl: 'views/employees.html',
    controller: 'employeesController'
  })
  .otherwise({
    redirectTo: '/'
  });

}]);

app.run(['$rootScope', '$location', '$cookies', '$http', function ($rootScope, $location, $cookies, $http){
  $rootScope.globals = $cookies.get('globals') || null;
  $rootScope.$on('$locationChangeStart', function (event, next, current) {
     var url = $location.url();
      var loggedIn = $rootScope.globals;
      if (url == '/register' && !loggedIn) {
          $location.path('/register');
      }else if (!loggedIn) {
          $location.path('/');
      }else if (url == '/newcomic' && loggedIn) {
          $location.path('/newcomic');
      }else if (url == '/' && loggedIn) {
          $location.path('/comics');
      }
  });
}]);
