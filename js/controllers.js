(function () {
  angular.module('comics.controllers',[])

  .controller('loginController', ['$rootScope','$location', '$cookies','employeeService', function ($rootScope, $location, $cookies, employeeService) {

      $rootScope.employee = {};
      $rootScope.message = "Mensaje 1";

      $rootScope.loginForm = function () {
        $location.path('/');
      }

      $rootScope.registerForm = function () {
        $location.path('/register');
      }

      $rootScope.login = function () {
          $rootScope.dataLoading = true;
          employeeService.authenticate($rootScope.employee.email, $rootScope.employee.password, function (response) {
              if (response.success) {
                  $cookies.put("globals", JSON.stringify($rootScope.employee));
                  $rootScope.globals = JSON.stringify($rootScope.employee);
                  $rootScope.employee = {};
                  $location.path('/comics');
              } else {
                  error(response.message);
              }
              $rootScope.dataLoading = false;
          });
      }

       $rootScope.register = function () {
           employeeService.addEmployee($rootScope.employee);
           $rootScope.employee = {};
           alert("Empleado registrado correctamente")
           $rootScope.loginForm();
        }

        $rootScope.error = function (message) {
             $rootScope.flash = {
                 message: message,
                 type: 'error'
             };
         }
    }])

  .controller('comicsController', ['$rootScope','$location', '$cookies','$scope', 'comicService', function ($rootScope, $location, $cookies, $scope, comicService) {

    var comics = [];
    $scope.showComics = true;
    $scope.comic = {};
    $scope.num = 9;

    $scope.newComic = function () {
      $location.path('/newcomic');
    }

    $scope.listComic = function () {
      $location.path('/comics');
    }

    $scope.loadComics = function () {
      $scope.num = $scope.num + 6;
    }

    comicService.all().then(function(data){
      $scope.comics = comics = data;
    })

    $scope.search = function () {
        var result = comics;
        if ($scope.searchTerm) {
          result = comics.filter(function (comic) {
            var title = comic && comic.title || "";
            return title.toLowerCase().indexOf($scope.searchTerm.toLowerCase()) !== -1;
          });
        }
        if(result.length < 9){
          $scope.num = result.length;
        }else{
          $scope.num = 9;
        }
        $scope.comics = result;
      }

      $scope.addComic = function () {
        $scope.comic.id = $scope.comics.length + 1;
        comicService.saveComic($scope.comic);
        $scope.comics.push($scope.comic);
        $scope.comic = {};
        $scope.listComic();
      }

      $rootScope.logOut = function () {
           $cookies.remove("globals");
           $rootScope.globals = null;
           $location.path('/');
       }
  }])

    .controller('comicController', ['$rootScope','$location', '$cookies','$scope', '$routeParams', 'comicService', function ($rootScope, $location, $cookies, $scope, $routeParams, comicService) {
        var id = $routeParams.id;
        $scope.comments = comicService.getComments($routeParams.id);
        $scope.comment = {};

        comicService.byId(id).then(function(data){
          $scope.comic = data;
        });

        $scope.addComment = function () {
          $scope.comment.date = Date.now();
          comicService.saveComment($scope.comic.id, $scope.comment);
          $scope.comments = comicService.getComments($routeParams.id);
          $scope.comment = {};
        };

        $rootScope.logOut = function () {
             $cookies.remove("globals");
             $rootScope.globals = null;
             $location.path('/');
         }

      }])

  .controller('employeesController', ['$rootScope','$location', '$cookies','employeeService', function ($rootScope, $location, $cookies, employeeService) {

    $rootScope.employees = {};
    var employees = [];

    $rootScope.employees = employees = employeeService.getEmployees();

    $rootScope.logOut = function () {
          $cookies.remove("globals");
          $rootScope.globals = null;
          $location.path('/');
        }

    }]);

})();
