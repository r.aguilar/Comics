(function(){
  angular.module('comics.services', [])
  .factory('employeeService', ['$http', '$q', '$window','$timeout', function ( $http, $q, $window, $timeout){

    var localStorage = $window.localStorage;

    function authenticate(email, password, callback) {
      $timeout(function () {
        var response;
        byEmail(email)
        .then(function (employee) {
          if (employee !== null && employee.password == password) {
            response = { success: true };
          } else {
            response = { success: false, message: 'Email o contraseña incorrecta' };
          }
          callback(response);
        });
      }, 1000);
    }

    function getEmployees(){
      var employees = localStorage.getItem("employees");
      if(!employees){
        employees = [];
        addEmployee({"name": "Rochy Aguilar", "email":"rochy@gmail.com", "password":"1234567890"});
      }
      employees = localStorage.getItem("employees");
      employees = JSON.parse(employees);
      return employees;
    }

    function addEmployee(employee){
      var employees = localStorage.getItem("employees");
      if(!employees){
        employees = [];
        employee.id = "1";
      }
      else{
        employees = JSON.parse(employees);
        employee.id = (employees.length + 1).toString();
      }

      employees.push(employee);
      localStorage.setItem("employees", JSON.stringify(employees));
    }

    function byEmail(email){
      var deferred = $q.defer();

      var results = getEmployees().filter(function (employee) {
        return employee.email == email;
      });
      if (results.length > 0) {
        deferred.resolve(results[0]);
      } else {
        deferred.reject();
      }
      return deferred.promise;
    }
    return {
      authenticate: authenticate,
      getEmployees: getEmployees,
      byEmail: byEmail,
      addEmployee : addEmployee
    }
  }])

  .factory('comicService', ['$http', '$q', '$window', function($http, $q, $window){

    var localStorage = $window.localStorage;

    function all(){
      var defered = $q.defer();
      var path = 'http://jsonplaceholder.typicode.com';
      $http.get(path + '/posts', { cache: true })
      .success(function (data) {
        var joinData = getComicsLocalStorage();
        joinData.forEach(function (comic) {
          data.push(comic);
        });
        defered.resolve(data);
      });
      return defered.promise;
    }

    function byId(id){
      var defered = $q.defer();
      var promise = defered.promise;
      all().then(function (data) {
        var results = data.filter(function (comic) {
          return comic.id == id;
        });

        if (results.length > 0) {
          defered.resolve(results[0]);
        } else {
          defered.reject();
        }
      });
      return promise;
  }

  function saveComic(comic){
    var comics = getComicsLocalStorage();
    comics.push(comic);
    localStorage.setItem("comics", JSON.stringify(comics));
  }

  function saveComment(comic, comment){
    var comments = getComments(comic);
    comments.push(comment);
    localStorage.setItem(comic, JSON.stringify(comments));
  }

  function getComicsLocalStorage(){
    var comics = localStorage.getItem("comics");
    if(!comics){
      comics = [];
    } else {
      comics = JSON.parse(comics);
    }
    return comics;
  }

  function getComments(comic){
    var comments = localStorage.getItem(comic);
    if(!comments){
      comments = [];
    } else {
      comments = JSON.parse(comments);
    }
    return comments;
  }

  return {
    all: all,
    byId: byId,
    saveComment: saveComment,
    getComments: getComments,
    saveComic : saveComic,
    getComicsLocalStorage : getComicsLocalStorage
  }
}]);
})();
